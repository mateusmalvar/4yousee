import {Component} from '@angular/core';
import {Http, Response, RequestOptions, Headers} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/finally';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = '4YouSee';
    filtro = '';
    lista = [];
    copyLista = [];
    private apiUrl = "https://private-7cf60-4youseesocialtest.apiary-mock.com/timeline";

    constructor(public http: Http) {

        this.getData()
            .subscribe(
            data => {this.lista = data; this.copyLista = data},
            error => {console.log(error);});
    }
    filtra() {
        let val = this.filtro;
        this.lista = this.copyLista;


        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.lista = this.lista.filter((item) => {
                return (item.category.toLowerCase().indexOf(val.toLowerCase()) > -1);
            })
        }
    };
    getData() {
        var retorno = this.http.get(this.apiUrl)
            .map(this.extractData)
            .catch(this.handleError)
            .finally(() => {
                console.log(this.lista);
            });
        return retorno;
    }
    private extractData(res: Response) {
        let body = res.json();
        return body || {};
    }

    private handleError(error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            console.log(error);
            const body = error.json() || '';
            errMsg = `${body.msg}`;
        } else {
            errMsg = error.msg ? error.msg : error.toString();
        }
        return Observable.throw(errMsg);
    }
}
